extern crate fps_clock;
extern crate sdl2;

#[derive(Copy, Clone)]
struct MyPoint {
    x: f64,
    y: f64,
    z: f64,
}

struct MyLine {
    p1: MyPoint,
    p2: MyPoint,
}

struct MyFloor {
    z: f64
}

struct MyCube {
    position: MyPoint,
    width: f64,
    depth: f64,
    heigth: f64,
    mass: f64,
    inertia: f64, // to be matrix?
    corners: Vec<MyPoint>,
    lines: Vec<MyLine>,
}
impl MyCube {
    pub fn new(pos: MyPoint, w: f64, d: f64, h: f64) -> MyCube {
        // lower level then upper, counter clockwise, starting closest left.
        //  vertex coords relative cube center/origin.
        let low = pos.z - h/2.0;
        let high = pos.z + h/2.0;
        let left = pos.x - w/2.0;
        let right = pos.x + w/2.0;
        let front = pos.y - d/2.0;
        let rear = pos.y + d/2.0;
        // Generate unlinked corner verticies
        let lfl = MyPoint{x: left, y: front, z: low};
        let rfl = MyPoint{x: right, y: front, z: low};
        let rrl = MyPoint{x: right, y: rear, z: low};
        let lrl = MyPoint{x: left, y: rear, z: low};
        let lfh = MyPoint{x: left, y: front, z: high};
        let rfh = MyPoint{x: right, y: front, z: high};
        let rrh = MyPoint{x: right, y: rear, z: high};
        let lrh = MyPoint{x: left, y: rear, z: high};

        let cube_corners: Vec<MyPoint> = vec!{ lfl, rfl, rrl, lrl, lfh, rfh, rrh, lrh };
        let l1 = MyLine{p1: lfl, p2: rfl};
        let l2 = MyLine{p1: rfl, p2: rfh};
        let l3 = MyLine{p1: rfh, p2: lfh};
        let l4 = MyLine{p1: lfh, p2: lfl};
        let l5 = MyLine{p1: lrl, p2: rrl};
        let l6 = MyLine{p1: rrl, p2: rrh};
        let l7 = MyLine{p1: rrh, p2: lrh};
        let l8 = MyLine{p1: lrh, p2: lrl};
        let l9 = MyLine{p1: lfl, p2: lrl};
        let l10 = MyLine{p1: rfl, p2: rrl};
        let l11 = MyLine{p1: rfh, p2: rrh};
        let l12 = MyLine{p1: lfh, p2: lrh};

        let cube_lines: Vec<MyLine> = vec!{ l1, l2, l3, l4, l5, l6, l7, l8, l9, l10, l11, l12 };

        let cube = MyCube{
            position: pos,
            width: w,
            depth: d,
            heigth: h,
            corners: cube_corners,
            lines: cube_lines,
            mass: 0.015,
            inertia: 1.0,
        };
        return cube
    }
    pub fn rotate_vec(&mut self, vec: (f64, f64, f64), deg: f64) {
        for p in &mut self.corners {
            rotate_around_vec(p, vec, deg);
        }
        for l in &mut self.lines {
            rotate_around_vec(&mut l.p1, vec, deg);
            rotate_around_vec(&mut l.p2, vec, deg);
        }
    }
    pub fn rotate_z(&mut self, deg: f64){
        for p in &mut self.corners {
            rotate_around_z(p, deg);
        }
        for l in &mut self.lines {
            rotate_around_z(&mut l.p1, deg);
            rotate_around_z(&mut l.p2, deg);
        }
    }
    pub fn rotate_x(&mut self, deg: f64){
        for p in &mut self.corners {
            rotate_around_x(p, deg);
        }
        for l in &mut self.lines {
            rotate_around_x(&mut l.p1, deg);
            rotate_around_x(&mut l.p2, deg);
        }
    }
    pub fn rotate_y(&mut self, deg: f64){
        for p in &mut self.corners {
            rotate_around_y(p, deg);
        }
        for l in &mut self.lines {
            rotate_around_y(&mut l.p1, deg);
            rotate_around_y(&mut l.p2, deg);
        }
    }

    pub fn translate(&mut self, dx: f64, dy:f64, dz:f64) {
        self.position.x = self.position.x + dx;
        self.position.y = self.position.y + dy;
        self.position.z = self.position.z + dz;
    }
}

struct MyCamera {
    position: MyPoint,
    direction: Vec<f64>,
    fov: f64,
}


struct MySimulator {
    sample_time_ms: u32,
    //enteties: Vec<MyCube>,
}


fn rotate_around_z(p: &mut MyPoint, deg: f64) -> () {
    let x = p.x;
    let y = p.y;
    let z = p.z;

    p.x = x*deg.cos() - y*deg.sin();
    p.y = x*deg.sin() + y*deg.cos();
    p.z = z;

}
fn rotate_around_x(p: &mut MyPoint, deg: f64) {
    let (x, y, z) = (p.x, p.y, p.z);
    p.x = x;
    p.y = y*deg.cos() - z*deg.sin();
    p.z = y*deg.sin() + z*deg.cos();
}
fn rotate_around_y(p: &mut MyPoint, deg: f64) {
    let (x,y,z) = (p.x, p.y, p.z);
    p.x = x*deg.cos() + z*deg.sin();
    p.y = y;
    p.z = -x*deg.sin() + z*deg.cos();
}

// To optimize, only normalize once and make fguntion assume it
fn rotate_around_vec(p: &mut MyPoint, vec: (f64, f64, f64), deg: f64) {
    
    let (x,y,z) = (p.x, p.y, p.z);

    // first normalize rot vec
    let u_norm = (vec.0*vec.0 + vec.1*vec.1 + vec.2*vec.2).sqrt();
    let u = (vec.0/u_norm, vec.1/u_norm, vec.2/u_norm);
    
    p.x = x*(deg.cos() + u.0*u.0*(1.0-deg.cos())) +
          y*(u.0*u.1*(1.0-deg.cos()) - u.2*deg.sin()) +
          z*(u.0*u.2*(1.0-deg.cos()) + u.1*deg.sin());
    p.y = x*(u.1*u.0*(1.0-deg.cos()) + u.2*deg.sin()) +
          y*(deg.cos() + u.1*u.1*(1.0-deg.cos())) +
          z*(u.1*u.2*(1.0-deg.cos()) - u.0*deg.sin());
    p.z = x*(u.2*u.0*(1.0-deg.cos()) - u.1*deg.sin()) +
          y*(u.2*u.1*(1.0-deg.cos()) + u.0*deg.sin()) +
          z*(deg.cos() + u.2*u.2*(1.0-deg.cos()) );
}



use sdl2::pixels::Color;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;

const win_h: u32 = 1200;
const win_w: u32 = 2000;
const FPS: u32 = 120;

fn main() {
    println!("Hello, world!");
    let mut fps = fps_clock::FpsClock::new(FPS);
    let mut nanosecs_since_last_tick: f32;

    let origin = MyPoint{x: 0f64, y: 0f64, z: 0f64};
    let mut cube1 = MyCube::new(origin, 100f64, 200f64, 300f64);
    let myfloor = MyFloor{z: -300.0};
    let cam1 = MyCamera{position: MyPoint{x: 0f64, y:-10f64, z:0.5f64}, direction: vec!{0f64,1f64,0f64}, fov: 90f64};

    let sim = MySimulator{sample_time_ms: 100u32}; //, enteties: vec![cube1]};
    
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem.window("robengine", win_w, win_h)
        .position_centered()
        .build()
        .unwrap();

    use sdl2::rect::Point;
    use sdl2::rect::Rect;
    let mut canvas = window.into_canvas().build().unwrap();

    canvas.set_draw_color(Color::RGB(255, 255, 255));
    canvas.clear();
    canvas.present();
    let mut event_pump = sdl_context.event_pump().unwrap();
    let mut i = 0;

    let mut cur_vel = (0.0f64, 0.0f64, 0.0f64);
    let acc = 0.3;
    let max_vel: f64 = 300.0;
    let mut x_acc: f64 = 0.0;
    let mut y_acc: f64 = 0.0;
    let mut z_acc: f64 = 0.0;
    let friction: f64 = 1.0-0.1;


    let mut rot_acc_x: f64 = 0.0;
    let mut rot_acc_y: f64 = 0.0;
    let mut rot_acc_z: f64 = 0.0;
    let rot_acc = 0.00025;
    let rot_friction = 1.0-0.01;
    
    let mut cur_rot_vel = (0.0f64, 0.0f64, 0.0f64);
    let max_rot_vel = 0.03;

    let g: f64 = 9.82;

    'running: loop {

        canvas.set_draw_color(Color::RGB(255,255,255));
        canvas.clear();
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    break 'running;
                },
                Event::KeyDown { keycode: Some(Keycode), .. } => {
                    //use sdl2::keyboard;
                    match Keycode {
                        Keycode::A  => { x_acc  = - acc; },
                        Keycode::D  => { x_acc  =   acc; },
                        Keycode::W  => { z_acc  =   acc; },
                        Keycode::S  => { z_acc  = - acc; },
                        Keycode::Q  => { y_acc  =   acc; },
                        Keycode::E  => { y_acc  = - acc; },
                        Keycode::Up     => { rot_acc_x = -rot_acc; },
                        Keycode::Down   => { rot_acc_x =  rot_acc; },
                        Keycode::Left   => { rot_acc_z = -rot_acc; },
                        Keycode::Right  => { rot_acc_z =  rot_acc; },
                        _ => {}


                    }
                },
                Event::KeyUp { keycode: Some(Keycode), .. } => {
                    match Keycode {
                        Keycode::A |
                        Keycode::D => {x_acc = 0.0;},
                        Keycode::W |
                        Keycode::S => {z_acc = 0.0; },
                        Keycode::Q |
                        Keycode::E => {y_acc = 0.0},
                        Keycode::Up     |
                        Keycode::Down   => { rot_acc_x =  0.0; },
                        Keycode::Left   |
                        Keycode::Right  => { rot_acc_z =  0.0; },

                        _ => {}
                    }
                }
                _ => {}
            }
        }

        // Draw corner pixels
        canvas.set_draw_color(Color::RGB(0, 0, 0));
        for p in &cube1.corners {
            let (col, row) = get_canvas_coords(cube1.position.x + p.x, cube1.position.y + p.y, cube1.position.z + p.z);
            let _ = canvas.draw_point(Point::new(col, row));
            // make corner pixels larger for visibility
            // ugly hack to make pixels smaller in rear
            let pix_d = 5 - (p.y*0.01).round() as i32;
            let _ = canvas.fill_rect(Rect::new(col-pix_d, row-pix_d, 2*pix_d as u32, 2*pix_d as u32));
        }

        //draw lines between pixels
        for l in &cube1.lines {
            if l.p1.y > 0.0 && l.p2.y > 0.0 {
                canvas.set_draw_color(Color::RGB(100,100,150));
            } else {
                canvas.set_draw_color(Color::RGB(0,200,50));
            }
            let (col1, row1) = get_canvas_coords(cube1.position.x + l.p1.x, cube1.position.y + l.p1.y, cube1.position.z + l.p1.z);
            let (col2, row2) = get_canvas_coords(cube1.position.x + l.p2.x, cube1.position.y + l.p2.y, cube1.position.z + l.p2.z);
            let p1 = Point::new(col1, row1);
            let p2 = Point::new(col2, row2);
            let _ = canvas.draw_line(p1, p2);
        }

        // draw floor
        canvas.set_draw_color(Color::RGB(0,0,0));
        let x_min = -1000;
        let x_max = 1000;
        let y_min = -1000;
        let y_max = 1000;
        for x in (x_min..x_max).step_by(100) {
            // draw lines along y
            let (col1, row1) = get_canvas_coords(x as f64, y_min as f64, myfloor.z);
            let (col2, row2) = get_canvas_coords(x as f64, y_max as f64, myfloor.z);
            let p1 = Point::new(col1, row1);
            let p2 = Point::new(col2, row2);
            let _ = canvas.draw_line(p1, p2);

            // draw lines along x
            let (col1, row1) = get_canvas_coords(x_min as f64, x as f64, myfloor.z);
            let (col2, row2) = get_canvas_coords(x_max as f64, x as f64, myfloor.z);
            let p1 = Point::new(col1, row1);
            let p2 = Point::new(col2, row2);
            let _ = canvas.draw_line(p1, p2);
        }

        i = i+1;
        
        // translation controls
        if cur_vel.0.abs() < max_vel{
            cur_vel.0 = cur_vel.0 + x_acc;
        }
        if cur_vel.1.abs() < max_vel{
            cur_vel.1 = cur_vel.1 + y_acc;
        }
        if cur_vel.2.abs() < max_vel{
            cur_vel.2 = cur_vel.2 + z_acc;
        }
        cube1.translate(cur_vel.0, cur_vel.1, cur_vel.2);

        cur_vel.0 = cur_vel.0*friction;
        cur_vel.1 = cur_vel.1*friction;
        cur_vel.2 = cur_vel.2*friction - cube1.mass*g;

        // check collision with floor
        let mut v = (1.0, 0.0, 0.0);
        let mut d = 0.0;
        for c in &mut cube1.corners {
            if c.z + &cube1.position.z < myfloor.z {
                println!("collision!");
                cur_vel.2 =  cur_vel.2.abs();
                // ROtation due to floor collision
                // Vector will always lie in plane of floor
                // Amount probably depends on fps
                // Vec is orthogonal to vec from c.g. to point projected onto floor
                //v = (1.0, 0.0, 0.0);
                let v_proj = ( c.x - &cube1.position.x, c.y - &cube1.position.y);
                let v = ( - v_proj.1, v_proj.0, 0.0);
                d = 1.0/FPS as f64;

            }
        }
        cube1.rotate_vec(v,d);
                

        // rotation control
        if cur_rot_vel.0.abs() < max_rot_vel {
            cur_rot_vel.0 = cur_rot_vel.0 + rot_acc_x;
        }
        if cur_rot_vel.1.abs() < max_rot_vel {
            cur_rot_vel.1 = cur_rot_vel.1 + rot_acc_y;
        }
        if cur_rot_vel.2.abs() < max_rot_vel {
            cur_rot_vel.2 = cur_rot_vel.2 + rot_acc_z;
        }
        cube1.rotate_x(cur_rot_vel.0);
        //cube1.rotate_y(cur_rot_vel.1); // not implemented in mycube yet.
        cube1.rotate_z(cur_rot_vel.2);

        cur_rot_vel.0 = cur_rot_vel.0 * rot_friction;
        cur_rot_vel.1 = cur_rot_vel.1 * rot_friction;
        cur_rot_vel.2 = cur_rot_vel.2 * rot_friction;


        canvas.present(); 
        nanosecs_since_last_tick = fps.tick();
        println!("loop ms: {}, freq: {}", nanosecs_since_last_tick*0.001, 1_000_000_000.0/nanosecs_since_last_tick);
    }


}

const focal_d: f64 = 5000.0;
pub fn get_canvas_coords(x: f64, y: f64, z: f64) -> (i32, i32) {
    let mid_x = win_w as f64 * 0.5;
    let mid_y = win_h as f64 * 0.5;
    
    // For infinite foccal length, i.e. no depth
    //let draw_x = mid_x + x;
    //let draw_y = mid_y - z;
    
    let draw_x = mid_x + focal_d * x / ( focal_d + y);
    let draw_y = mid_y - focal_d * z / ( focal_d + y);


    let col = draw_x.round() as i32;
    let row = draw_y.round() as i32;
    return (col, row);
}












