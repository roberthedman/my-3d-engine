# My 3D Engine
Without further hesitation, how hard could it be?

Turns out, just colission detection is a whole field in itself.

## Introduction
This is a small working playground that started as a simple 3d cube renderer written in rust, with the help of sdl2, which has slowly been getting more features such as FOV adjusting, controls to translate and rotate the cube, and a floor.

## Usage
`cargo run` ought to be sufficient.
ESC quits the program, WASD translates the cube, and the arrows rotate it.

## Future development
There is a floor, but colission is garbage. Torque is asserted onto the cube from the floor but it's not working well.
That's where next focus might be, or to rewrite the physics part of the engine.
The physics is loosely based on realistic differential equations, with emphasis on loosely.
Or one could focus on making it able to import any shape and render it instead of the hard coded cube.

But hey, the cube spins! (If you want it to).